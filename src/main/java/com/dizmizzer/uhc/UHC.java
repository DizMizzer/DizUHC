package com.dizmizzer.uhc;

import com.dizmizzer.uhc.Commands.Console.Kick;
import com.dizmizzer.uhc.Commands.Console.Scenarios;
import com.dizmizzer.uhc.Listeners.ChatListener;
import com.dizmizzer.uhc.Listeners.DeathListeners;
import com.dizmizzer.uhc.Listeners.ScenarioEvent.AnimalKillListener;
import com.dizmizzer.uhc.Listeners.ScenarioEvent.BreakListener;
import com.dizmizzer.uhc.Listeners.ScenarioEvent.CraftListener;
import com.dizmizzer.uhc.Listeners.ScenarioEvent.PlayerKillListener;
import com.dizmizzer.uhc.Listeners.SmallStuffListener;
import com.dizmizzer.uhc.Listeners.WorldListener;
import com.dizmizzer.uhc.Managers.GameManager;
import com.dizmizzer.uhc.Managers.SQLManager;
import com.dizmizzer.uhc.Managers.SettingsManager;
import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.Util.GameState;
import com.dizmizzer.uhc.Util.Scenario;
import com.erezbiox1.CommandsAPI.CommandManager;
import net.minecraft.server.v1_8_R1.BiomeBase;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class UHC extends JavaPlugin implements Listener{

    public HashMap<Scenario, Boolean> scen;
    public boolean debug = true;
    public ArrayList<String> customWhitelist = new ArrayList<>();
    public GameState gameState;
    public void onEnable() {
        registerHashMaps();
        GameManager.getInstance().setup(this);
        enableScenarios(GameManager.getInstance().getScenarios());
        overrideBiome();
        registerEvents();
        registerCommands();
        registerManagers();
        FinishHostSetup();
        gameState = GameState.SETUP;
    }

    private void FinishHostSetup() {

        customWhitelist.addAll(Arrays.asList(GameManager.getInstance().getMods()));
        customWhitelist.add(GameManager.getInstance().getHost().toLowerCase());

    }

    @EventHandler
    public void onPing(ServerListPingEvent e) {
                e.setMotd(ChatColor.translateAlternateColorCodes('&',GameManager.getInstance().getMOTD(gameState)));
    }

    public void enableScenarios(ArrayList<Scenario> s) {
        for (Scenario sc : s) {
            scen.put(sc, true);
        }

        for (Scenario sc : Scenario.values()) {
            if (!scen.containsKey(sc)) {
                scen.put(sc, false);
            }
        }
    }
    private void registerHashMaps() {
        scen = new HashMap<>();
    }

    public HashMap<Scenario, Boolean> getScen() {
        return scen;
    }

    private void registerManagers() {

        StringManager.get().setup(this);
        SettingsManager.get().setup(this);
        //SQLManager.getInstance().connect();
    }

    private void register(Listener e) {
        getServer().getPluginManager().registerEvents(e, this);
    }

    private void registerEvents() {
        register(new WorldListener(this));
        register(new SmallStuffListener(this, this));
        register(new ChatListener());
        register(new DeathListeners());
        register(this);
        //Scenarios
        register(new BreakListener(this));
        register(new CraftListener(this));
        register(new PlayerKillListener(this));
        register(new AnimalKillListener(this));
    }

    private void registerCommands() {


        CommandManager.register(new Kick(), new Scenarios(this));
    }

    public void onDisable() {
        //SQLManager.getInstance().closeConnection();
    }

    private void overrideBiome() {
        try {
            Field biomesFiled = BiomeBase.class.getDeclaredField("biomes");
            biomesFiled.setAccessible(true);

            if (biomesFiled.get(null) instanceof BiomeBase[]) {
                BiomeBase[] biomes = (BiomeBase[]) biomesFiled.get(null);
                biomes[BiomeBase.DEEP_OCEAN.id] = BiomeBase.PLAINS;
                biomes[BiomeBase.OCEAN.id] = BiomeBase.FOREST;
            }


        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
