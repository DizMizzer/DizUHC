package com.dizmizzer.uhc.Commands.Player.Moderate;

import com.dizmizzer.uhc.Util.DizUtils;
import com.erezbiox1.CommandsAPI.Command;
import com.erezbiox1.CommandsAPI.CommandListener;
import org.bukkit.command.CommandSender;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class StartGame extends DizUtils implements CommandListener {

    @Command
    public void start(CommandSender s, String[] args) {
    }
}
