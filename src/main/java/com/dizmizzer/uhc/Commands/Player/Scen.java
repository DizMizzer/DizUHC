package com.dizmizzer.uhc.Commands.Player;

import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.DizUtils;
import com.dizmizzer.uhc.Util.Scenario;
import com.erezbiox1.CommandsAPI.Command;
import com.erezbiox1.CommandsAPI.CommandListener;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class Scen extends DizUtils implements CommandListener {

    Inventory inv = Bukkit.createInventory(null, 27);
    ArrayList<ItemStack> enabled = new ArrayList<>();

    UHC u;

    public Scen(UHC uhc) {
        this.u = uhc;
    }

    @Command
    public void scen(CommandSender player, String[] args) {
        for (Scenario s : Scenario.values()) {
            if (u.getScen().containsKey(s)) {
                if (u.getScen().get(s))
                    sendMessage(player, " " + s.toString() + ":" + s.getDescription(), true);
            }
        }
    }

}
