package com.dizmizzer.uhc.Commands.Console;

import com.dizmizzer.uhc.Managers.StringManager;
import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.DizUtils;
import com.dizmizzer.uhc.Util.Scenario;
import com.erezbiox1.CommandsAPI.Command;
import com.erezbiox1.CommandsAPI.CommandListener;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.command.CommandSender;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class Scenarios extends DizUtils implements CommandListener {

    UHC u;
    public Scenarios(UHC uhc) {
        this.u = uhc;
    }

    @Command(permission = "uhc.toggle")
    public void toggle(CommandSender player, String[] args) {
        if (args.length < 1) {
            sendMessage(player, StringManager.get().getString("notenoughargs"), false);
            return;
        }
        Scenario s = Scenario.valueOf(args[0]);
        if (s == null) {
            sendMessage(player, StringManager.get().getString("notexist"), false);
            return;
        }

        if (args.length == 1) {
            if (u.getScen().containsKey(s)) {
                if (u.getScen().get(s)) {
                    u.getScen().put(s, false);
                    sendScenMessage(player, StringManager.get().getString("scendisabled"), s);
                } else {
                    u.getScen().put(s, true);
                    sendScenMessage(player, StringManager.get().getString("scenenabled"), s);

                }

            } else {
                u.getScen().put(s, true);
                sendScenMessage(player, StringManager.get().getString("scenenabled"), s);

            }
        } else {

            String[] accepted = {"true", "false", "on", "off", "yes", "no"};

            Boolean b = ArrayUtils.contains(accepted, args[1]);
            if (!b) {
                sendMessage(player, StringManager.get().getString("invalid"), false);

                return;
            }
            Boolean bo = BooleanUtils.isTrue(BooleanUtils.toBoolean(args[1]));
            if (bo) {
                u.getScen().put(s, true);
                sendScenMessage(player, StringManager.get().getString("scenenabled"), s);

            } else {
                u.getScen().put(s, false);
                sendScenMessage(player, StringManager.get().getString("scendisabled"), s);

            }
        }
    }
}
