package com.dizmizzer.uhc.Listeners.ScenarioEvent;

import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.Scenario;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

public class BowShootListener implements Listener {

    private UHC uhc;

    public BowShootListener(UHC u) {
        this.uhc = u;
    }
    @EventHandler
    public void onShot(EntityShootBowEvent e) {
        if (!(e.getEntity() instanceof Player)) return;

        if (uhc.scen.get(Scenario.TRIPLEARROW)) {
            e.getEntity().launchProjectile(Arrow.class);
            e.getEntity().launchProjectile(Arrow.class);
        }

    }
    @EventHandler
    public void onArrowHit(ProjectileHitEvent event){
        if(event.getEntity() instanceof Arrow && uhc.scen.get(Scenario.TRIPLEARROW)){
            Arrow arrow = (Arrow) event.getEntity();
            arrow.remove();
        }
    }
}
