package com.dizmizzer.uhc.Listeners.ScenarioEvent;

import com.dizmizzer.uhc.UHC;
import com.dizmizzer.uhc.Util.Scenario;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class BreakListener implements Listener {

    HashMap<Material, Material> itemdrop = new HashMap<>();
    UHC u;
    ArrayList<UUID> hasBlock = new ArrayList<>();
    public BreakListener(UHC uhc) {

        itemdrop.put(Material.IRON_ORE, Material.IRON_INGOT);
        itemdrop.put(Material.GOLD_ORE, Material.GOLD_INGOT);

        this.u = uhc;
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (u.scen.containsKey(Scenario.HALFORES)) {
            if (u.scen.get(Scenario.HALFORES)) {
                if (!hasBlock.contains(e.getPlayer().getUniqueId())) {
                    e.setCancelled(true);
                    e.getBlock().setType(Material.AIR);
                    e.getPlayer().giveExp(e.getExpToDrop());
                    hasBlock.add(e.getPlayer().getUniqueId());
                    return;
                }
            }
        }

        if (u.scen.containsKey(Scenario.CUTCLEAN)) {
            if (u.scen.get(Scenario.CUTCLEAN)) {

                if (itemdrop.containsKey(e.getBlock().getType())) {
                    e.setCancelled(true);

                } else {
                    return;
                }
                int amount = e.getExpToDrop();

                e.getPlayer().giveExp(amount);
                e.getBlock().setType(Material.AIR);
                e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(itemdrop.get(
                        e.getBlock().getType()),
                        ThreadLocalRandom.current().nextInt(1, 4)
                ));

            }
        }

    }


}
