package com.dizmizzer.uhc.Managers;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class SettingsManager {

    static SettingsManager instance = new SettingsManager();
    File file;
    FileConfiguration config;

    public static SettingsManager get() {
        return instance;
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void setup(Plugin p) {
        if (!p.getDataFolder().exists()) {
            p.getDataFolder().mkdir();
        }

        file = new File(p.getDataFolder(), "SQL.yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            config = YamlConfiguration.loadConfiguration(file);
            config.set("host", "localhost");
            config.set("username", "root");
            config.set("port", 3306);
            config.set("password", "password");
            config.set("database", "minecraftdb");
            try {
                config.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        config = YamlConfiguration.loadConfiguration(file);

    }

}
