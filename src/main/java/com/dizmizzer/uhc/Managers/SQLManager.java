package com.dizmizzer.uhc.Managers;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.UUID;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class SQLManager {
    static SQLManager instance = new SQLManager();
    Connection con;

    public static SQLManager getInstance() {
        return instance;
    }

    public synchronized void connect() {
        con = null;

        FileConfiguration cf = SettingsManager.get().getConfig();
        String url = "jdbc:mysql://" + cf.getString("host") + ":" + cf.getInt("port")+"/"+ cf.getString("database");
        String user = cf.getString("username");
        String password = cf.getString("password");

        try {
            con = DriverManager.getConnection(url, user, password);
            Bukkit.getLogger().info("Connected!");
            PreparedStatement pst = con.prepareStatement("CREATE TABLE IF NOT EXISTS `PlayerData` (kills int, " +
                    "deaths int, " +
                    "UUID varchar(255), " +
                    "Name varchar(255), " +
                    "wins int, " +
                    "loses int, " +
                    "points int, " +
                    "IP varchar(255) )");
            pst.executeUpdate();

            Bukkit.getLogger().info("CONNECTED!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void closeConnection() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void addPlayer(Player p) {
        try {
            String name = p.getName();
            String UUID = p.getUniqueId().toString();
            String IP = p.getAddress().getAddress().getHostAddress();

            PreparedStatement pst = con.prepareStatement("SELECT UUID FROM `PlayerData` WHERE UUID = '" + p.getUniqueId() + "'");
            ResultSet rs = pst.executeQuery();

            if (!rs.absolute(1)) {
                pst = con.prepareStatement("INSERT INTO `PlayerData` (kills, deaths, UUID, Name, wins, loses, points, IP) VALUES (0, 0, '" + UUID + "', '" + name + "', 0, 0, 0, '" + IP + "')");
                pst.executeUpdate();
            } else {
                pst = con.prepareStatement("UPDATE `PlayerData` SET IP = '" + IP + "', Name = '" + name + "' WHERE UUID = '" + UUID + "'");
                pst.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public synchronized void addKill(UUID id) {
        try {
            PreparedStatement pst = con.prepareStatement("UPDATE `PlayerData` SET kills = kills + 1 WHERE UUID='" + id + "'");
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
