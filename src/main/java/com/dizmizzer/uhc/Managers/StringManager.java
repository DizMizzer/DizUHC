package com.dizmizzer.uhc.Managers;

import com.dizmizzer.uhc.Util.PunishType;
import com.dizmizzer.uhc.Util.Scenario;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public class StringManager {
    public static String Prefix = toColor("&e[&6DizUHC&e]");
    static StringManager instance = new StringManager();
    File file;
    FileConfiguration config;
    HashMap<String, String> msg = new HashMap<>();

    public static StringManager get() {
        return instance;
    }

    public static String toColor(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public void setup(Plugin p) {
        msg.put("prefix","&e[&6DizUHC&e]");
        msg.put("chatlayout","[%rank%]%player%: %chat%");
        msg.put("notenoughargs", "Not enough arguments!");
        msg.put("ondeathkill","%player% has been killed by %killer% with %hearts% health left.");
        msg.put("ondeathfall","%player% has felt to his death.");
        msg.put("onkill","You have killed %player%");
        msg.put("onwin","%player% has won the game!");
        msg.put("onban", "%punisher% has banned %player% for: %reason%");
        msg.put("onkick", "%punisher% has kicked %player% for: %reason%");
        msg.put("noperm","&cYou don't have permission to do this.");
        msg.put("invalid", "&cYour input is invalid.");
        msg.put("scenenabled", "&c%scenario% has been enabled.");
        msg.put("scendisabled", "&c%scenario% has been disabled.");
        msg.put("playeronly", "Only players can use this.");
        msg.put("worldcreate", "&aSuccesfully recreated UHC world.");
        msg.put("ScenarioNull", "&cScenario %scenario% not found!");
        msg.put("ScenarioDefault", "&c&l%scenario%");
        msg.put("alreadyOpen","&cServer is already open!");
        if (!p.getDataFolder().exists()) p.getDataFolder().mkdir();
        file = new File(p.getDataFolder(), "messages.yml");

        if (! file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        config = YamlConfiguration.loadConfiguration(file);

        for (String s : msg.keySet()) {
            if (! config.contains(s)) {
                config.set(s, msg.get(s));
            }

        }
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }


        msg.clear();
    }

    public String kickPlayer(PunishType pt, String msg, Player player, CommandSender kicker) {
        if (kicker instanceof Player) {
            return toColor(config.getString(pt.getMsg())).replace("%player%", player.getName()).replace("%punisher%", kicker.getName()).replace("%reason", msg);

        } else {
            return toColor(config.getString(pt.getMsg())).replace("%player%", player.getName()).replace("%punisher%", "Console").replace("%reason", msg);

        }
    }

    public String getKillString(String s, Player killed, Player killer) {
        return null;
    }

    public String getString(String s, Player p) {
        return toColor(s.replace("%player%",p.getDisplayName()).replace("%chat%",s));
    }

    public String getString(String s) {
        return toColor(config.getString("prefix") + " " + config.getString(s));
    }

    public void reloadConfig() {
        try {
            config.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public String getString(String s, String sc) {
        return toColor(config.getString("prefix") + " " + config.getString(s).replace("%scenario%", sc));
    }

}
