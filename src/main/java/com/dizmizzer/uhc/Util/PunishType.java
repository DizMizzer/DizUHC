package com.dizmizzer.uhc.Util;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public enum PunishType {

    KICK("onkick"),
    BAN("onban"),
    MUTE("onmute");

    String a;

    PunishType(String prefix) {
        this.a = prefix;
    }

    public String getMsg() {
        return a;
    }


}
