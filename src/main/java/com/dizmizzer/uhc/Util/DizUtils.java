package com.dizmizzer.uhc.Util;

import com.dizmizzer.uhc.Managers.StringManager;
import org.bukkit.command.CommandSender;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public abstract class DizUtils {

    public static void sendMessage(CommandSender p, String m, Boolean prefix) {
        if (!prefix) {
            p.sendMessage(StringManager.Prefix + StringManager.toColor(m));
        } else {
            p.sendMessage(StringManager.toColor(m));
        }
    }

/*    public static void chatMsg(Player player, String m) {
        player.sendMessage(StringManager.get().getString(m, player));
    } */

    public static void sendScenMessage(CommandSender cs, String m, Scenario s) {
        cs.sendMessage((m).replace("%scenario%", s.toString()));

    }


}
