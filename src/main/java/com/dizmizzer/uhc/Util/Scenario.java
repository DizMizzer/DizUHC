package com.dizmizzer.uhc.Util;

import org.bukkit.Material;

/**
 * Created by DizMizzer.
 * Everybody has permission to copy the
 * code and change it however they want.
 * You don't have permission to copy
 * the code and claim it as your own
 * unless stated by the Developer.
 */
public enum Scenario {

    BOWLESS("bowless", "&7You are unable to craft a bow.", Material.BOW),
    CUTCLEAN("cutclean", "&7All ores you mine and mobs you kill will be cooked automatically.", Material.FURNACE),
    HALFORES("1/2 ores", "&7For every 2 ores you mine, 1 drops.", Material.IRON_PICKAXE),
    GOLDLESS("Goldless", "", Material.GOLD_INGOT),
    DIAMONDLESS("Diamondless", "", Material.DIAMOND),
    TRIPLEARROW("3x arrow","When you shoot with a bow, not one, but 3 arrows will come out.", Material.ARROW);

    String desc;
    String toString;
    Material material;

    Scenario(String toString, String desc, Material m) {
        this.toString = toString;
        this.desc = desc;
        this.material = m;
    }

    @Override
    public String toString() {
        return toString;
    }

    public String getDescription() {
        return desc;
    }

    public Material getMaterial() {
        return material;
    }
}
