//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.erezbiox1.CommandsAPI;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.lang.reflect.Method;
import java.util.*;

public class CommandManager {
    private static final String wildcardSymbol = "*";
    private static final String numberWildcardSymbol = "**";
    private static final String eitherSymbol = "|";
    private static final String errorPrefix;
    private static final String permissionErrorDefault;
    private static final String playerErrorDefault;
    private static final String argumentsErrorDefault;

    static {
        errorPrefix = ChatColor.RED + "" + ChatColor.BOLD + "Sorry!" + ChatColor.RESET + ChatColor.GRAY + " ";
        permissionErrorDefault = errorPrefix + "You don't have permission to access this command.";
        playerErrorDefault = errorPrefix + "You must be a player to access this command.";
        argumentsErrorDefault = errorPrefix + "Invalid Arguments, Please try again!";
    }

    public CommandManager() {
    }

    public static void register(CommandListener... listeners) {
        CommandListener[] var1 = listeners;
        int var2 = listeners.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            CommandListener listener = var1[var3];
            Class<?> _class = listener.getClass();
            Method[] classMethods = _class.getMethods();
            Map<String, Set<Method>> map = new HashMap();
            Method[] var8 = classMethods;
            int var9 = classMethods.length;

            for(int var10 = 0; var10 < var9; ++var10) {
                Method method = var8[var10];
                if(method.isAnnotationPresent(Command.class)) {
                    List<String> names = new ArrayList();
                    if(!method.getAnnotation(Command.class).name().isEmpty()) {
                        Command command = method.getAnnotation(Command.class);
                        if(command.name().trim().contains(" ")) {
                            names.addAll(new ArrayList(Arrays.asList(command.name().trim().split(" "))));
                        } else {
                            names.add(command.name());
                        }
                    } else if(_class.isAnnotationPresent(CommandClass.class) && !_class.getAnnotation(CommandClass.class).name().isEmpty()) {
                        CommandClass command = _class.getAnnotation(CommandClass.class);
                        if(command.name().trim().contains(" ")) {
                            names.addAll(new ArrayList(Arrays.asList(command.name().trim().split(" "))));
                        } else {
                            names.add(command.name());
                        }
                    } else {
                        names.add(method.getName());
                    }

                    Iterator var17 = names.iterator();

                    while(var17.hasNext()) {
                        String name = (String)var17.next();
                        if(!map.containsKey(name)) {
                            Set<Method> methods = new HashSet();
                            methods.add(method);
                            map.put(name, methods);
                        } else {
                            map.get(name).add(method);
                        }
                    }
                }
            }

            registerCommands(listener, map);
        }

    }

    private static void registerCommands(CommandListener listener, Map<String, Set<Method>> map) {
        map.forEach((name, methods) -> {
            CommandManager.CustomCommand var10001 = new CommandManager.CustomCommand(name) {
                public boolean execute(CommandSender sender, String cmd, String[] args) {
                    String error = "";
                    Iterator var5 = methods.iterator();

                    while(var5.hasNext()) {
                        Method method = (Method)var5.next();
                        Command command = method.getAnnotation(Command.class);
                        String permission = command.permission();
                        String arguments = command.arguments();
                        String permissionError = command.permissionError();
                        String playerError = command.playerError();
                        String argumentsError = command.argumentsError();
                        if(listener.getClass().isAnnotationPresent(CommandClass.class)) {
                            CommandClass commandClass = method.getClass().getAnnotation(CommandClass.class);
                            if(permission.isEmpty()) {
                                permission = commandClass.permission();
                            } else {
                                permission = permission + commandClass.permission();
                            }

                            if(permissionError.equals("default")) {
                                permissionError = commandClass.permissionError();
                            }

                            if(playerError.equals("default")) {
                                playerError = commandClass.playerError();
                            }

                            if(argumentsError.equals("default")) {
                                argumentsError = commandClass.argumentsError();
                            }
                        }

                        if(permissionError.equals("default")) {
                            permissionError = CommandManager.permissionErrorDefault;
                        }

                        if(playerError.equals("default")) {
                            playerError = CommandManager.playerErrorDefault;
                        }

                        if(argumentsError.equals("default")) {
                            argumentsError = CommandManager.argumentsErrorDefault;
                        }

                        boolean player = method.getParameterTypes().length != 0 && method.getParameterTypes()[0].equals(Player.class);
                        if(!CommandManager.playerCheck(player, sender)) {
                            if(!playerError.equalsIgnoreCase("none") && !playerError.isEmpty()) {
                                error = playerError;
                            }
                        } else if(!CommandManager.permissionCheck(permission, sender)) {
                            if(!playerError.equalsIgnoreCase("none") && !permissionError.isEmpty()) {
                                error = permissionError;
                            }
                        } else if(!CommandManager.argumentsCheck(arguments, args)) {
                            if(!argumentsError.equalsIgnoreCase("none") && !argumentsError.isEmpty()) {
                                error = argumentsError;
                            }
                        } else {
                            CommandManager.run(method, arguments, args, listener, sender, player);
                            error = "";
                        }
                    }

                    if(!error.equals("")) {
                        sender.sendMessage(error);
                    }

                    return true;
                }
            };
        });
    }

    private static void run(Method method, String arguments, String[] args, CommandListener listener, CommandSender sender, boolean player) {
        try {
            int parameters = method.getParameterTypes().length;
            if(parameters == 0) {
                method.invoke(listener);
            } else if(parameters == 1) {
                if(!method.getParameterTypes()[0].equals(Player.class) && !method.getParameterTypes()[0].equals(CommandSender.class)) {
                    if(arguments.isEmpty()) {
                        method.invoke(listener, new Object[]{args});
                    } else {
                        method.invoke(listener, new Object[]{getArgs(arguments, args)});
                    }
                } else {
                    method.invoke(listener, sender);
                }
            } else if(parameters == 2) {
                if(!method.getParameterTypes()[0].equals(Player.class) && !method.getParameterTypes()[0].equals(CommandSender.class)) {
                    if(!arguments.isEmpty()) {
                        method.invoke(listener, getArgs(arguments, args), sender);
                    } else {
                        method.invoke(listener, args, sender);
                    }
                } else if(arguments.isEmpty()) {
                    method.invoke(listener, sender, args);
                } else {
                    method.invoke(listener, sender, getArgs(arguments, args));
                }
            }
        } catch (Exception var7) {
            var7.printStackTrace();
        }

    }

    private static boolean permissionCheck(String permissions, CommandSender sender) {
        if(!sender.isOp() && !permissions.isEmpty()) {
            if(permissions.trim().contains(" ")) {
                String[] var2 = permissions.trim().split(" ");
                int var3 = var2.length;

                for(int var4 = 0; var4 < var3; ++var4) {
                    String permission = var2[var4];
                    if(!sender.hasPermission(permission)) {
                        return false;
                    }
                }
            } else if(!sender.hasPermission(permissions)) {
                return false;
            }

            return true;
        } else {
            return true;
        }
    }

    private static boolean playerCheck(boolean player, CommandSender sender) {
        return !player || sender instanceof Player;
    }

    private static boolean argumentsCheck(String arguments, String[] args) {
        if(arguments.isEmpty()) {
            return true;
        } else {
            String[] wildcards = arguments.split(" ");
            if(wildcards.length != args.length) {
                return false;
            } else {
                for(int i = 0; i < wildcards.length; ++i) {
                    if(!wildcards[i].equals("*")) {
                        if(wildcards[i].contains("|")) {
                            String[] var4 = wildcards[i].split("\\|");
                            int var5 = var4.length;

                            for(int var6 = 0; var6 < var5; ++var6) {
                                String o = var4[var6];
                                if(!o.equalsIgnoreCase(args[i])) {
                                    return false;
                                }
                            }
                        }

                        if(wildcards[i].contains("**") && !isNumber(wildcards[i])) {
                            return false;
                        }

                        if(!wildcards[i].toLowerCase().equals(args[i].toLowerCase())) {
                            return false;
                        }
                    }
                }

                return true;
            }
        }
    }

    private static String[] getArgs(String arguments, String[] args) {
        String[] wildcards = arguments.split(" ");
        List<String> list = new ArrayList();

        for(int i = 0; i < wildcards.length; ++i) {
            if(wildcards[i].equals("*") || wildcards[i].contains("|") || wildcards[i].equals("**")) {
                list.add(args[i]);
            }
        }

        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }

    private static boolean isNumber(String string) {
        return string.matches("-?\\d+(\\.\\d+)?");
    }

    abstract static class CustomCommand extends BukkitCommand {
        CustomCommand(String name) {
            super(name);

            try {
                SimpleCommandMap smp = (SimpleCommandMap)Class.forName("org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + ".CraftServer").getMethod("getCommandMap", new Class[0]).invoke(Bukkit.getServer());
                smp.register("dizuhc", this);
                this.register(smp);
            } catch (Exception var3) {
            }

        }
    }
}
